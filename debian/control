Source: python-x2go
Section: python
Priority: optional
Maintainer: Debian Remote Maintainers <debian-remote@lists.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends:
 cdbs,
 debhelper (>= 10),
 dh-python,
 python3,
 python3-setuptools,
 python3-gevent,
 python3-paramiko,
 python3-xlib,
 python3-sphinx,
 locales
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://wiki.x2go.org
Vcs-Git: https://salsa.debian.org/debian-remote-team/python-x2go.git
Vcs-Browser: https://salsa.debian.org/debian-remote-team/python-x2go

Package: python3-x2go
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 python3-gevent (>= 0.13.6),
 python3-paramiko (>= 1.15.1),
 python3-requests,
 python3-simplejson,
 python3-xlib,
 nxproxy | qvd-nxproxy,
Recommends:
 cups-bsd | lpr,
 pulseaudio,
 sshfs,
 x2gokdriveclient,
Suggests:
 telekinesis-client,
 mteleplayer-clientside,
Description: Python module providing X2Go client API (Python 3)
 X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick
 .
 This Python module allows you to integrate X2Go client
 support into your Python applications by providing a
 Python-based X2Go client API (for Python 3).

Package: python-x2go-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 libjs-jquery,
 libjs-underscore,
Multi-Arch: foreign
Description: Python module providing X2Go client API (documentation)
 X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick
 .
 This package contains the Python X2Go client API
 documentation generated with Epydoc.
